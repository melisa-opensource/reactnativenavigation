# Installation

1. Install dependencies
```bash
npm i --save @react-navigation/native react-native-reanimated react-native-gesture-handler react-native-screens react-native-safe-area-context @react-native-community/masked-view @react-navigation/stack
```

2. Pod install
```bash
# with npm run
npm run pod
# or manual commands
cd ios/
pod install
cd ..
```

3. Run app
```bash
npm run ios
# or android
npm run android
```

# Documentation

1. [Reactnavigation.org](https://reactnavigation.org/docs/en/hello-react-navigation.html)